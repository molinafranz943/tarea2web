document.getElementById("registroForm").addEventListener("submit", function(event) {
    event.preventDefault();
    
    var nombre = document.getElementById("nombre").value;
    var apellido = document.getElementById("apellido").value;
    var curso = document.getElementById("curso").value;
    
    var table = document.getElementById("datosTabla").getElementsByTagName("tbody")[0];
    var newRow = table.insertRow(table.rows.length);
    
    var cell1 = newRow.insertCell(0);
    var cell2 = newRow.insertCell(1);
    var cell3 = newRow.insertCell(2);
    
    cell1.innerHTML = nombre;
    cell2.innerHTML = apellido;
    cell3.innerHTML = curso;
    
    document.getElementById("registroForm").reset();
});
